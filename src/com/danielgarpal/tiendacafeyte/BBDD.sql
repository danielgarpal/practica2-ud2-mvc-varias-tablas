

-- Base de datos sobre una tienda de cafes y te. La tabla proveedores registra los proveedores de la tienda.
-- La tabla productos, al crear un producto, se necesita enlazar con un proveedor.
-- la tabla venta, al crear una venta, se necesita enlazar con un producto.

CREATE DATABASE IF NOT EXISTS CafeTeria;
--
USE CafeTeria;
--
CREATE TABLE IF NOT EXISTS proveedores(
idProveedor INT auto_increment PRIMARY KEY,
nombreProveedor VARCHAR(50) NOT NULL,
direccion VARCHAR(150) NOT NULL,
pais VARCHAR(50) NOT NULL,
telefono VARCHAR(9) NOT NULL,
descripcion VARCHAR(100)
);
--

CREATE TABLE IF NOT EXISTS productos (
idproducto INT auto_increment PRIMARY KEY,
nombreProducto VARCHAR(50) ,
idProveedor INT NOT NULL,
tipo VARCHAR(4) NOT NULL,
origen VARCHAR(20) NOT NULL,
fechaEmpaquetado DATE NOT NULL,
intensidad INT NOT NULL,
FOREIGN KEY (idProveedor) REFERENCES proveedores(idProveedor)
);
--

CREATE TABLE IF NOT EXISTS ventas(
idpedido INT auto_increment PRIMARY KEY,
idproducto INT NOT NULL,
precio FLOAT NOT null,
descuentoSocio boolean NOT NULL,
cantidad INT NOT NULL,
total FLOAT NOT NULL,
fechaVenta DATE NOT NULL,
FOREIGN KEY (idproducto) REFERENCES productos(idproducto)
);
--

delimiter ||
create function existeNombreProveedor(f_name varchar(50))
returns bit
begin
	declare i int;
    set i = 0;
    while ( i < (select max(idProveedor) from proveedores)) do
    if  ((select nombreProveedor from proveedores where idProveedor = (i + 1)) like f_name) then return 1;
    end if;
    set i = i + 1;
    end while;
    return 0;
end; ||
delimiter ;
--
delimiter ||
create function existeNombreProducto(f_name varchar(50))
returns bit
begin
	declare i int;
    set i = 0;
    while ( i < (select max(idproducto) from productos)) do
    if  ((select nombreProducto from productos where idproducto = (i + 1)) like f_name) then return 1;
    end if;
    set i = i + 1;
    end while;
    return 0;
end; ||
delimiter ;

