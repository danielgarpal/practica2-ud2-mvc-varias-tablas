package com.danielgarpal.tiendacafeyte.main;

import com.danielgarpal.tiendacafeyte.gui.Controlador;
import com.danielgarpal.tiendacafeyte.gui.Modelo;
import com.danielgarpal.tiendacafeyte.gui.Vista;
/*
Clase Principal del programa
 */
public class Principal {
//Clase main del programa, que crea una nueva vista, un nuevo modelo y un controlador con la
    //vista y el modelo
    public static void main(String[] args) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista, modelo);
    }

}
