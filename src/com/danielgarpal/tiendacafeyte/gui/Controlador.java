package com.danielgarpal.tiendacafeyte.gui;


import com.danielgarpal.tiendacafeyte.util.Util;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;

public class Controlador  implements ActionListener, ListSelectionListener {

    private Modelo modelo;
    private Vista vista;
    boolean refrescado;

    //Constructor de la clase
    public Controlador(Vista vista, Modelo modelo) {
        this.modelo = modelo;
        this.vista = vista;
        modelo.conectar();

        addActionListeners(this);
        addItemListeners(this);
        //addWindowListeners(this);
        refrescarTodo();
        borrarCamposTodo();
        setOptions();
        addListSelectionListeners();
    }

    //Método que añade los listeners de lista a las tablas
    private void  addListSelectionListeners() {

        vista.tablaProveedor.getSelectionModel().addListSelectionListener(this);
        vista.tablaVenta.getSelectionModel().addListSelectionListener(this);
        vista.tablaProd.getSelectionModel().addListSelectionListener(this);

    }

    //método que escribe en la ventana de opciones los valores actuales
    private void setOptions() {
        vista.optionDialog.tfIP.setText(modelo.getIp());
        vista.optionDialog.tfUsuario.setText(modelo.getUser());
        vista.optionDialog.pfContrasenia.setText(modelo.getPassword());
        vista.optionDialog.pfContraseniaAdmin.setText(modelo.getAdminPassword());
    }


    //REFRESCAR
    //método que llama a todos los métodos de refrescar
    private void refrescarTodo() {
        refrescarProveedores();
        refrescarProductos();
        refrescarVentas();
        refrescado = false;


    }
    //Método que establece un modelo actualizado de la bbdd a la tabla de ventas
    private void refrescarVentas() {
        try {
            vista.tablaVenta.setModel(construirTableModelVentas(modelo.consultarVenta()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    //Método que establece un modelo actualizado de la bbdd a la tabla de productos y actualiza el combobox
    private void refrescarProductos() {
        try {
            vista.tablaProd.setModel(construirTableModelProductos(modelo.consultarProducto()));
            vista.cbProductoVent.removeAllItems();
            for (int i = 0; i < vista.dtmProductos.getRowCount(); i++) {
                vista.cbProductoVent.addItem(vista.dtmProductos.getValueAt(i, 0) + " - " +
                        vista.dtmProductos.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    //Método que establece un modelo actualizado de la bbdd a la tabla de proveedores y actualiza el combobox
    private void refrescarProveedores() {
        try {
            vista.tablaProveedor.setModel(construirTableModelProveedores(modelo.consultarProveedor()));
            vista.cbProveedorProd.removeAllItems();
            for (int i = 0; i < vista.dtmProveedores.getRowCount(); i++) {
                vista.cbProveedorProd.addItem(vista.dtmProveedores.getValueAt(i, 0) + " - " + vista.dtmProveedores.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    //CONSTRUIR TABLE MODELS
    /*
    Método que organiza el dtm
     */
    private TableModel construirTableModelVentas(ResultSet consultarVenta)  throws SQLException {

        ResultSetMetaData metaData = null;

        metaData = consultarVenta.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(consultarVenta, columnCount, data);
        vista.dtmVentas.setDataVector(data, columnNames);
        return vista.dtmVentas;


    }

    /*
        Método que organiza el dtm
         */
    private TableModel construirTableModelProductos(ResultSet consultarProducto)  throws SQLException {

        ResultSetMetaData metaData = null;

        metaData = consultarProducto.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(consultarProducto, columnCount, data);
        vista.dtmProductos.setDataVector(data, columnNames);
        return vista.dtmProductos;


    }

    /*
        Método que organiza el dtm
         */
    private TableModel construirTableModelProveedores(ResultSet consultarProveedor) throws SQLException {

        ResultSetMetaData metaData = null;

            metaData = consultarProveedor.getMetaData();

            // names of columns
            Vector<String> columnNames = new Vector<>();
            int columnCount = metaData.getColumnCount();
            for (int column = 1; column <= columnCount; column++) {
                columnNames.add(metaData.getColumnName(column));
            }
            // data of the table
            Vector<Vector<Object>> data = new Vector<>();
            setDataVector(consultarProveedor, columnCount, data);
            vista.dtmProveedores.setDataVector(data, columnNames);
            return vista.dtmProveedores;


    }

    /*
        Método que organiza un vector para usarlo en dtm
         */
    private void setDataVector(ResultSet result, int columnCount, Vector<Vector<Object>> data) throws SQLException {
        while (result.next()) {
            Vector<Object> vector = new Vector<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(result.getObject(columnIndex));
            }
            data.add(vector);
        }
    }


    /*
    método que añade lísteners a los elementos
     */
    private void addActionListeners(ActionListener listener) {
        vista.bAniadirProv.addActionListener(listener);
        vista.bAniadirProv.setActionCommand("aniadirProveedor");
        vista.bModificarProv.addActionListener(listener);
        vista.bModificarProv.setActionCommand("modificarProveedor");
        vista.bEliminarProv.addActionListener(listener);
        vista.bEliminarProv.setActionCommand("eliminarProveedor");

        vista.bAniadirProd.addActionListener(listener);
        vista.bAniadirProd.setActionCommand("aniadirProducto");
        vista.bModificarProd.addActionListener(listener);
        vista.bModificarProd.setActionCommand("modificarProducto");
        vista.bEliminarProd.addActionListener(listener);
        vista.bEliminarProd.setActionCommand("eliminarProducto");

        vista.bAniadirVent.addActionListener(listener);
        vista.bAniadirVent.setActionCommand("aniadirVenta");
        vista.bModificarVent.addActionListener(listener);
        vista.bModificarVent.setActionCommand("modificarVenta");
        vista.bEliminarVent.addActionListener(listener);
        vista.bEliminarVent.setActionCommand("eliminarVenta");
        vista.bValidar.addActionListener(listener);
        vista.bValidar.setActionCommand("abrirOpciones");
        vista.optionDialog.bGuardar.addActionListener(listener);
        vista.optionDialog.bGuardar.setActionCommand("guardarOpciones");

        vista.bTotal.addActionListener(listener);
        vista.bTotal.setActionCommand("calcularTotal");




    }



    /*
        método que añade lísteners a los elementos del menú
         */
    private void addItemListeners(ActionListener listener) {
        vista.itemOpciones.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.itemDesconectar.addActionListener(listener);

    }




    /*
    Método del lístener, actúa según el action command recibido
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        String command = e.getActionCommand();

        switch (command){
            //Hace visible el menú que pide la contraseña
            case "Opciones":vista.adminPasswordDialog.setVisible(true);
            break;
            //Cierra la conexión
            case "Desconectar":modelo.desconectar();
            break;
            //Cierra el programa
            case "Salir": System.exit(0);
            break;
            //Método que llama el boton validar de la contraseña. Si esta es correcta abre la ventana de opciones
            case "abrirOpciones":
                if (String.valueOf(vista.adminPassword.getPassword()).equals(modelo.getAdminPassword())){
                    vista.adminPassword.setText("");
                    vista.adminPasswordDialog.dispose();
                    vista.optionDialog.setVisible(true);

                }else{
                    Util.showErrorAlert("La contraseña introducida no es correcta");
                }
            break;
                /*
                Guarda las opciones de conexión modificadas por el usuario  en el fichero values.
                Manda los nuevos ajustes al método setPropValues()
                 */
            case "guardarOpciones":
                modelo.setPropValues(vista.optionDialog.tfIP.getText(),
                    vista.optionDialog.tfpuerto.getText(),
                    vista.optionDialog.tfUsuario.getText(),
                    String.valueOf(vista.optionDialog.pfContrasenia.getPassword()),
                    String.valueOf(vista.optionDialog.pfContraseniaAdmin.getPassword()));
                vista.optionDialog.dispose();
                vista.dispose();
                new Controlador(new Vista(), new Modelo());



                break;
                /*
                Comprueba que los campos de proveedor no están vacios, que el proveedor no existe en la
                base de datos e introduce en el método insertar proveedor los datos del proveedor
                 */
            case "aniadirProveedor":
                try {
                    if (comprobarProveedorVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tablaProveedor.clearSelection();
                    } else if (modelo.proveedorExiste(vista.tfNombreProv.getText())) {
                        Util.showErrorAlert("Ese nombre ya existe.\nIntroduce un proveedor diferente");
                        vista.tablaProveedor.clearSelection();
                    } else {
                        modelo.insertarProveedor(vista.tfNombreProv.getText(),
                                vista.tfDescProv.getText(),
                                vista.tfDireccionProv.getText(),
                                vista.tfPaisProv.getText(),
                                vista.tfTelefonoProv.getText()
                                );
                        refrescarProveedores();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.tablaProveedor.clearSelection();
                }
                borrarCamposProveedores();
            break;
            /*
                Comprueba que los campos de producto no están vacios, que el producto no existe en la
                base de datos e introduce en el método insertar producto los datos del proveedor
                 */
            case "aniadirProducto":

                if (comprobarProductoVacio()) {
                    Util.showErrorAlert("Rellena todos los campos");
                    vista.tablaProd.clearSelection();
                } else if (modelo.productoExiste(vista.tfNombreProducto.getText())) {
                    Util.showErrorAlert("Ese producto ya existe.\nIntroduce un producto diferente");
                    vista.tablaProd.clearSelection();
                } else {
                    if(vista.rbCafeProd.isSelected()){

                        modelo.insertarProducto(vista.tfNombreProducto.getText(),
                                String.valueOf(vista.cbProveedorProd.getSelectedItem()),
                                "cafe",
                                vista.tfOrigenProd.getText(),
                                vista.dpFechaEmpaquetadoProd.getDate(),
                                vista.sIntensidadProd.getValue()
                        );
                    }else{
                        modelo.insertarProducto(vista.tfNombreProducto.getText(),
                                String.valueOf(vista.cbProveedorProd.getSelectedItem()),
                                "te",
                                vista.tfOrigenProd.getText(),
                                vista.dpFechaEmpaquetadoProd.getDate(),
                                vista.sIntensidadProd.getValue()
                        );
                    }

                    refrescarProductos();
                }

                borrarCamposProductos();
            break;
            /*
                Comprueba que los campos de venta no están vacios
                e introduce en el método insertar venta los datos del proveedor
                 */
            case "aniadirVenta":
                try {
                if (comprovarVentaVacio()) {
                    Util.showErrorAlert("Rellena todos los campos");
                    vista.tablaVenta.clearSelection();
                } else {
                    if(vista.rbSiVenta.isSelected()){
                        modelo.insertarVenta(
                                String.valueOf(vista.cbProductoVent.getSelectedItem()),
                                vista.tfPrecioVent.getText(),
                                true,
                                vista.tfCantidadVent.getText(),
                                vista.dpFehcaVentaVent.getDate(),
                                vista.tfTotal.getText()
                        );
                    }else{
                        modelo.insertarVenta(
                                String.valueOf(vista.cbProductoVent.getSelectedItem()),
                                vista.tfPrecioVent.getText(),
                                false,
                                vista.tfCantidadVent.getText(),
                                vista.dpFehcaVentaVent.getDate(),
                                vista.tfTotal.getText()
                        );
                    }

                    refrescarVentas();
                }
                borrarCamposVentas();
            } catch (NumberFormatException nfe) {
                Util.showErrorAlert("Introduce números en los campos que lo requieren");
                vista.tablaProveedor.clearSelection();
            }

            break;
                /*
                Comprueba que los campos de proveedor no están vacios e inserta en
                modificarProveedor los datos a modificar
                 */
            case "modificarProveedor":

                try {
                    if (comprobarProveedorVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tablaProveedor.clearSelection();

                    } else {
                        modelo.modificarProveedor(vista.tfNombreProv.getText(),
                                vista.tfDescProv.getText(),
                                vista.tfDireccionProv.getText(),
                                vista.tfPaisProv.getText(),
                                vista.tfTelefonoProv.getText(),
                                Integer.parseInt((String) vista.tablaProveedor.getValueAt(vista.tablaProveedor.getSelectedRow(),0))
                        );
                        refrescarProveedores();
                        borrarCamposProveedores();

                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.tablaProveedor.clearSelection();
                }


                break;
                /*
                Comprueba que los campos de producto no están vacios e inserta en
                modificarProducto los datos a modificar
                 */
            case "modificarProducto":


                try {
                    if (comprobarProductoVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tablaProd.clearSelection();

                    } else {
                        if (vista.rbCafeProd.isSelected()) {

                            modelo.modificarProducto(vista.tfNombreProducto.getText(),
                                    String.valueOf(vista.cbProveedorProd.getSelectedItem()),
                                    "cafe",
                                    vista.tfOrigenProd.getText(),
                                    vista.dpFechaEmpaquetadoProd.getDate(),
                                    vista.sIntensidadProd.getValue(),
                                    Integer.parseInt((String) vista.tablaProd.getValueAt(vista.tablaProd.getSelectedRow(), 0)));
                        } else {
                            modelo.modificarProducto(vista.tfNombreProducto.getText(),
                                    String.valueOf(vista.cbProveedorProd.getSelectedItem()),
                                    "te",
                                    vista.tfOrigenProd.getText(),
                                    vista.dpFechaEmpaquetadoProd.getDate(),
                                    vista.sIntensidadProd.getValue(),
                                    Integer.parseInt((String) vista.tablaProd.getValueAt(vista.tablaProd.getSelectedRow(), 0)));

                        }
                            refrescarProveedores();
                            borrarCamposProveedores();

                        }
                    } catch(NumberFormatException nfe){
                        Util.showErrorAlert("Introduce números en los campos que lo requieren");
                        vista.tablaProveedor.clearSelection();
                    }


                    break;
                /*
                Comprueba que los campos de venta no están vacios e inserta en
                modificarVenta los datos a modificar
                 */
                    case "modificarVenta":
                        try {
                            if (comprovarVentaVacio()) {
                                Util.showErrorAlert("Rellena todos los campos");
                                vista.tablaVenta.clearSelection();
                            } else {
                                if(vista.rbSiVenta.isSelected()){
                                    modelo.modificarVenta(
                                            String.valueOf(vista.cbProductoVent.getSelectedItem()),
                                            vista.tfPrecioVent.getText(),
                                            true,
                                            vista.tfCantidadVent.getText(),
                                            vista.dpFehcaVentaVent.getDate(),
                                            vista.tfTotal.getText(),
                                            Integer.parseInt((String) vista.tablaVenta.getValueAt(vista.tablaVenta.getSelectedRow(), 0)));


                                }else{
                                    modelo.modificarVenta(
                                            String.valueOf(vista.cbProductoVent.getSelectedItem()),
                                            vista.tfPrecioVent.getText(),
                                            false,
                                            vista.tfCantidadVent.getText(),
                                            vista.dpFehcaVentaVent.getDate(),
                                            vista.tfTotal.getText(),
                                            Integer.parseInt((String) vista.tablaVenta.getValueAt(vista.tablaVenta.getSelectedRow(), 0)));


                                }

                                refrescarVentas();
                            }
                            borrarCamposVentas();
                        } catch (NumberFormatException nfe) {
                            Util.showErrorAlert("Introduce números en los campos que lo requieren");
                            vista.tablaProveedor.clearSelection();
                        }


                        break;
                        //Dirige al método eliminar el id seleccionado
                    case "eliminarProveedor":

                        modelo.eliminarProveedor(Integer.parseInt((String) vista.tablaProveedor.getValueAt(
                                vista.tablaProveedor.getSelectedRow(), 0)));

                        borrarCamposProveedores();
                        refrescarProveedores();
                        break;
                        //Dirige al método eliminar el id seleccionado
                    case "eliminarProducto":
                        modelo.eliminarProducto(Integer.parseInt((String) vista.tablaProd.getValueAt(
                                vista.tablaProd.getSelectedRow(), 0)));
                        borrarCamposProductos();
                        refrescarProductos();
                        break;
                        //Dirige al método eliminar el id seleccionado
                    case "eliminarVenta":

                        modelo.eliminarVenta(Integer.parseInt((String) vista.tablaVenta.getValueAt(
                                vista.tablaVenta.getSelectedRow(), 0)));

                        borrarCamposVentas();
                        refrescarVentas();
                        break;

                        //método que calcula el total de la venta antes de añadirlo a la base de datos
                    case "calcularTotal":
                        float t = 0;
                        try {
                            t = (Integer.parseInt(vista.tfPrecioVent.getText()) * Integer.valueOf(vista.tfCantidadVent.getText()));
                            if (vista.rbSiVenta.isSelected()) {
                                t = (float) (t * 0.95);
                            }
                            vista.tfTotal.setText(String.valueOf(t));
                        } catch (NumberFormatException ex) {
                            Util.showErrorAlert("Debes introducir valores numéricos en 'Cantidad' y 'Precio'");
                            vista.tfPrecioVent.setText("");
                            vista.tfCantidadVent.setText("");
                        }

                        break;



        }

    }



    private void borrarCamposTodo() {
        borrarCamposProductos();
        borrarCamposProveedores();
        borrarCamposVentas();
    }
    /*
    Borra los campos
     */
    private void borrarCamposProductos() {
        vista.tfNombreProducto.setText("");
        vista.tfOrigenProd.setText("");
        vista.dpFechaEmpaquetadoProd.setText("");
        vista.sIntensidadProd.setValue(1);
        vista.cbProveedorProd.setSelectedIndex(-1);
        vista.rbCafeProd.setSelected(true);
    }
    /*
        Borra los campos
         */
    private void borrarCamposVentas() {
        vista.cbProductoVent.setSelectedIndex(-1);
        vista.tfPrecioVent.setText("");
        vista.rbNoVenta.setSelected(true);
        vista.tfCantidadVent.setText("");
        vista.dpFehcaVentaVent.setText("");
        vista.tfTotal.setText("");
    }
    /*
    Borra los campos
     */
    private void borrarCamposProveedores() {
        vista.tfNombreProv.setText("");
        vista.tfDireccionProv.setText("");
        vista.tfPaisProv.setText("");
        vista.tfTelefonoProv.setText("");
        vista.tfDescProv.setText("");
    }
    /*
    Comprueba que los campos no están vacíos
     */
    private boolean comprobarProductoVacio() {
        if(vista.cbProveedorProd.getSelectedIndex() == -1 || vista.tfOrigenProd.getText().isEmpty()
                || vista.tfNombreProducto.getText().isEmpty() || vista.dpFechaEmpaquetadoProd.getText().isEmpty()){
            return true;
        }else{
            return false;
        }

    }
    /*
    Comprueba que los campos no están vacíos
     */
    private boolean comprobarProveedorVacio() {
if(vista.tfNombreProv.getText().isEmpty() || vista.tfDireccionProv.getText().isEmpty()  ||
        vista.tfPaisProv.getText().isEmpty()   ||  vista.tfTelefonoProv.getText().isEmpty()  || vista.tfDescProv.getText().isEmpty() ){
    return true;
}else{
    return false;
}
    }
    /*
     Comprueba que los campos no están vacíos
     */
    private boolean comprovarVentaVacio() {
        if(vista.tfPrecioVent.getText().isEmpty() || vista.cbProductoVent.getSelectedIndex() == -1  ||
                vista.tfCantidadVent.getText().isEmpty() || vista.dpFehcaVentaVent.getText().isEmpty() || vista.tfTotal.getText().isEmpty()) {
            if(vista.tfTotal.getText().isEmpty()){
                Util.showInfoAlert("Para rellenar el campo total, pulsa el botón 'Calcular Total' ");
            }
            return true;
        }else{
            return false;
        }

    }


    /*
    Listener de las tablas. Se activa cuando seleccionas un elemento de la tabla
    y rellena los datos de la fila seleccionada en la interfaz
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting() && !((ListSelectionModel) e.getSource()).isSelectionEmpty()) {
            if (e.getSource().equals(vista.tablaProveedor.getSelectionModel())) {

                //rellena los datos de proveedor
                int row = vista.tablaProveedor.getSelectedRow();
                vista.tfNombreProv.setText(String.valueOf(vista.tablaProveedor.getValueAt(row, 1)));
                vista.tfDireccionProv.setText(String.valueOf(vista.tablaProveedor.getValueAt(row, 2)));
                vista.tfPaisProv.setText(String.valueOf(vista.tablaProveedor.getValueAt(row, 3)));
                vista.tfTelefonoProv.setText(String.valueOf(vista.tablaProveedor.getValueAt(row, 4)));
                vista.tfDescProv.setText(String.valueOf(vista.tablaProveedor.getValueAt(row, 5)));

            } else if (e.getSource().equals(vista.tablaProd.getSelectionModel())) {
                int row = vista.tablaProd.getSelectedRow();
                vista.tfNombreProducto.setText(String.valueOf(vista.tablaProd.getValueAt(row, 1)));
                vista.cbProveedorProd.setSelectedItem(String.valueOf(vista.tablaProd.getValueAt(row, 2)));
                String cafeOTe=(String.valueOf(vista.tablaProd.getValueAt(row, 3)));
                //Comprueba si el tipo es cafe o te, y selecciona el radio button necesario en la vista
                if( cafeOTe.equalsIgnoreCase("cafe")) {
                vista.rbCafeProd.setSelected(true);
                }else{
                    vista.rbTeProd.setSelected(true);
                }
                vista.tfOrigenProd.setText(String.valueOf(vista.tablaProd.getValueAt(row, 4)));
                vista.dpFehcaVentaVent.setDate((Date.valueOf(String.valueOf(vista.tablaProd.getValueAt(row, 5)))).toLocalDate());
                int valorSlider=Integer.parseInt((String) vista.tablaProd.getValueAt(row, 6));
                vista.sIntensidadProd.setValue(valorSlider);

            } else if (e.getSource().equals(vista.tablaVenta.getSelectionModel())) {
                int row = vista.tablaVenta.getSelectedRow();


                vista.cbProductoVent.setSelectedItem(String.valueOf(vista.tablaVenta.getValueAt(row, 1)));
                vista.tfPrecioVent.setText(String.valueOf(vista.tablaVenta.getValueAt(row, 2)));
                boolean tieneDescuento= Boolean.parseBoolean(String.valueOf(vista.tablaVenta.getValueAt(row, 3)));
                //Selecciona si o no en la vista
                if (tieneDescuento==true){
                    vista.rbSiVenta.setSelected(true);
                }else{
                    vista.rbNoVenta.setSelected(true);
                }
                vista.tfCantidadVent.setText(String.valueOf(vista.tablaVenta.getValueAt(row, 4)));
                vista.dpFehcaVentaVent.setDate((Date.valueOf(String.valueOf(vista.tablaVenta.getValueAt(row, 6)))).toLocalDate());
                vista.tfTotal.setText(String.valueOf(vista.tablaVenta.getValueAt(row, 5)));

            } else if (e.getValueIsAdjusting() && ((ListSelectionModel) e.getSource()).isSelectionEmpty() && !refrescado) {
                if (e.getSource().equals(vista.tablaProveedor.getSelectionModel())) {
                    borrarCamposProveedores();
                } else if (e.getSource().equals(vista.tablaProd.getSelectionModel())) {
                    borrarCamposProductos();
                } else if (e.getSource().equals(vista.tablaVenta.getSelectionModel())) {
                    borrarCamposVentas();
                }
            }

        }
    }
}
