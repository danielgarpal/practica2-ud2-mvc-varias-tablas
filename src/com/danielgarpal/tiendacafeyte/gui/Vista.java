package com.danielgarpal.tiendacafeyte.gui;

import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

/*
Clase vista, la cual tiene toda la interfaz  y la funcionalidad de los table models y el menu
 */

public class Vista extends JFrame {
    private final static String TITULOFRAME = "Aplicación CafeTería";
    public JPanel panel;
    public JTabbedPane tabbedPane1;

    //Proveedor
    public JTextField tfNombreProv;
    public JTextField tfDireccionProv;
    public JTextField tfTelefonoProv;
    public JTextField tfDescProv;
    public JButton bAniadirProv;
    public JButton bModificarProv;
    public JButton bEliminarProv;
    public JTable tablaProveedor;
    public JTextField tfPaisProv;

    //Producto
    public JComboBox cbProveedorProd;
    public JSlider sIntensidadProd;
    public JTextField tfOrigenProd;
    public JTextField tfNombreProducto;
    public JButton bAniadirProd;
    public JButton bModificarProd;
    public JButton bEliminarProd;
    public JTable tablaProd;
    public JRadioButton rbTeProd;
    public JRadioButton rbCafeProd;
    public DatePicker dpFechaEmpaquetadoProd;

    //Venta
    public JButton bAniadirVent;
    public JButton bModificarVent;
    public JButton bEliminarVent;
    public JTextField tfPrecioVent;
    public JTable tablaVenta;
    public JComboBox cbProductoVent;
    public JTextField tfCantidadVent;
    public JRadioButton rbNoVenta;
    public JRadioButton rbSiVenta;
    public DatePicker dpFehcaVentaVent;
    public JTextField tfTotal;
    public JButton bTotal;
    public JLabel Total;

    //Menu
    public JMenuItem itemOpciones;
    public JMenuItem itemDesconectar;
    public JMenuItem itemSalir;

    //Dialog
    public ConfigDialog optionDialog;
    public JDialog adminPasswordDialog;
    public JButton bValidar;
    public JPasswordField adminPassword;

    //DEFAULT TABLE MODEL
    DefaultTableModel dtmProveedores;
    DefaultTableModel dtmProductos;
    DefaultTableModel dtmVentas;

    //Constructor de la clase vista, que utiliza el título de la aplicación y se dirige al método que inicia el panel
    public Vista() {
        super(TITULOFRAME);
        initFrame();
    }



    //Método que inicia el panel
    private void initFrame() {
        this.setContentPane(panel);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        this.setSize(new Dimension(this.getWidth() + 200, this.getHeight() + 100));
        //this.setLocationRelativeTo(this);
        this.setLocationRelativeTo(null);

        //Hago no editable el campo del precio total
        tfTotal.setEditable(false);

        //Creo una ventana de opciones
        optionDialog=new ConfigDialog(this);

        setMenu();
        setAdminDialog();
        setTableModels();



    }

    /*
    Menu que crea table models para las tres tabla y las añade a ellas
     */
    private void setTableModels() {
        this.dtmVentas=new DefaultTableModel();
        this.tablaVenta.setModel(dtmVentas);
        this.dtmProductos=new DefaultTableModel();
        this.tablaProd.setModel(dtmProductos);
        this.dtmProveedores=new DefaultTableModel();
        this.tablaProveedor.setModel(dtmProveedores);
    }

    /*
    Método que abre un dialogo para pedir la contraseña. Al darle al botón
    de Validar, abre la opción del controlador que comprueba la contraseña para continuar
     */
    private void setAdminDialog() {
        bValidar = new JButton("Validar");
        adminPassword=new JPasswordField();
        adminPassword.setPreferredSize(new Dimension(100,30));
        Object[] options = new Object[]{adminPassword,bValidar};
        JOptionPane pane = new JOptionPane("Introduce la contraseña",
                JOptionPane.WARNING_MESSAGE,JOptionPane.YES_NO_OPTION,null,options);
        adminPasswordDialog=new JDialog(this,"Opciones",true);
        adminPasswordDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        adminPasswordDialog.setContentPane(pane);
        adminPasswordDialog.pack();
        adminPasswordDialog.setLocationRelativeTo(this);


    }

    /*
    Método que crea un menú barra para guardar opciones como modificar los datos de conexión o salir
     */
    private void setMenu() {
        //Creo nueva barra de menú
        JMenuBar menuBar = new JMenuBar();

        //Creo un menú y le pongo título
        JMenu menu = new JMenu("Menu");


        //Items del menú y actiion commands

        //Opciones
        itemOpciones = new JMenuItem("Opciones");
        itemOpciones.setActionCommand("Opciones");

        //Desconectar
        itemDesconectar = new JMenuItem("Desconectar");
        itemDesconectar.setActionCommand("Desconectar");

        // Salir
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");

        //Añadir items al menú
        menu.add(itemOpciones);
        menu.add(itemDesconectar);
        menu.add(itemSalir);

        //Añadir el menú a la barra de menú
        menuBar.add(menu);

        //Central en horizontal
        menuBar.add(Box.createHorizontalGlue());

        //Añadir la barra de menú a la vista
        this.setJMenuBar(menuBar);


    }


}
