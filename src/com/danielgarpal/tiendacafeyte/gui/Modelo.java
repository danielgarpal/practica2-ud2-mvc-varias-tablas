package com.danielgarpal.tiendacafeyte.gui;

import com.danielgarpal.tiendacafeyte.util.Util;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;


import javax.swing.*;
import java.io.*;
import java.sql.*;
import java.time.LocalDate;
import java.util.Properties;

/*
Clase modelo, que contiene la funcionalidad con la base de datos y con la conexión
 */

public class Modelo {
    private String ip;
    private String user;
    private String password;
    private String adminpassword;
    private String puerto;

    private Connection conexion;

    public Modelo() {
          getPropValues();

    }
    // GETTERS
    public String getIp() { return ip; }

    public String getUser() { return user; }

    public String getPassword() { return password; }

    public String getAdminPassword() { return adminpassword; }




    /*
    Método conectar, que intenta realizar la conexión con la base de datos a través de unos parámetros guardados en un fichero
     */
    public void conectar() {
        try {
            conexion = DriverManager.getConnection("jdbc:mysql://" + ip
                    + ":"+puerto+"/CafeTeria",user,password);
        } catch (SQLException e) {
            Util.showErrorAlert("No se ha podido realizar la conexión");
        }


    }


    //Método que cierra la conexión
    public void desconectar() {
        try {
            conexion.close();
            conexion = null;
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    //Método que guarda en las variables los parámetros de conexión del fichero properties
    private void getPropValues() {
        InputStream inputStream = null;
        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";
            inputStream = new FileInputStream(propFileName);
            prop.load(inputStream);
            //Guarda los valores en las variables
            ip = prop.getProperty("ip");
            user = prop.getProperty("user");
            password = prop.getProperty("pass");
            adminpassword = prop.getProperty("admin");
            puerto=prop.getProperty("puerto");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null)
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }

    /*
Método que ejecuta una sentencia a la base de datos para obtener todos sus campos
     */
    public ResultSet consultarProveedor() throws SQLException {
        String sentenciaSql = "SELECT concat(idProveedor) AS 'ID', concat(nombreProveedor) AS 'Nombre Proveedor', " +
                "concat(direccion) AS 'Direccion', concat(telefono) AS 'Teléfono',concat(pais) AS 'País'," +
                "concat(descripcion) AS 'Descripcion' FROM proveedores";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        //devuelve la consulta
        return resultado;
    }
    /*
Método que ejecuta una sentencia a la base de datos para obtener todos sus campos
     */
    public ResultSet consultarProducto() throws SQLException {
        String sentenciaSql = "SELECT concat(prod.idproducto) AS 'ID',concat(prod.nombreProducto) AS 'Nombre', concat(prov.idProveedor + '-' + prov.nombreProveedor ) AS 'Proveedor', " +
                "concat(prod.tipo) AS 'Tipo', concat(prod.origen) AS 'Origen',concat(prod.fechaEmpaquetado) AS 'Fecha Empaquetado'," +
                "concat(intensidad) AS 'Intensidad'" +
                "FROM productos AS prod " +
                "INNER JOIN proveedores AS prov ON prod.idProveedor = prov.idProveedor";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }
    /*
Método que ejecuta una sentencia a la base de datos para obtener todos sus campos
     */
    public ResultSet consultarVenta() throws SQLException {
        String sentenciaSql = "SELECT concat(v.idpedido) AS 'ID', concat(prod.idproducto + '-' + prod.nombreProducto ) AS 'Producto', " +
                "concat(v.precio) AS 'Precio', concat(v.descuentoSocio) AS 'Descuento socio',concat(v.cantidad) AS 'Cantidad'," +
                "concat(v.total) AS 'Total',concat(v.fechaVenta) AS 'Fecha Venta'" +
                "FROM ventas AS v " +
                "INNER JOIN productos AS prod ON prod.idproducto = v.idproducto";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /*
   Método que ejecuta una consulta almacenada en la base de datos para confirmar si existe un proveedor con el mismo nombre
    */
    public boolean proveedorExiste(String text) {

        String authorNameConsult = "SELECT existeNombreProveedor(?)";
        PreparedStatement function;
        boolean nameExists = false;
        try {
            function = conexion.prepareStatement(authorNameConsult);
            function.setString(1, text);
            ResultSet rs = function.executeQuery();
            rs.next();

            nameExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nameExists;
    }
    /*
   Método que ejecuta una consulta almacenada en la base de datos para confirmar si existe un producto con el mismo nombre
    */
    public boolean productoExiste(String text) {

        String authorNameConsult = "SELECT existeNombreProducto(?)";
        PreparedStatement function;
        boolean nameExists = false;
        try {
            function = conexion.prepareStatement(authorNameConsult);
            function.setString(1, text);
            ResultSet rs = function.executeQuery();
            rs.next();

            nameExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nameExists;
    }

    /*
Método que ejecuta una sentencia para insertar un proveedor en la base de datos con los valores introducidos por parámetro
     */
    public void insertarProveedor(String nombre, String desc, String direc, String pais, String telef) {
        //preparo la sentencia
        String sentenciaSql = "INSERT INTO proveedores (nombreProveedor," +
                " direccion, pais, telefono, descripcion) " +
                "VALUES (?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;


        //Se introducen en la sentencia los valores
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, direc);
            sentencia.setString(3, pais);
            sentencia.setString(4, telef);
            sentencia.setString(5, desc);

            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }


    /*
    Método que cambia los valores del fichero properties para que el usuario pueda modificar los valores de conexión
     */
    public void setPropValues(String ip, String tfpuerto, String user, String ps, String psa) {
        this.ip=ip;
        this.user=user;
        this.password=ps;
        this.adminpassword=psa;
        this.puerto=tfpuerto;

        try {
        Properties prop = new Properties();
        prop.setProperty("ip", ip);
        prop.setProperty("puerto",tfpuerto);
        prop.setProperty("user", user);
        prop.setProperty("pass", ps);
        prop.setProperty("admin", psa);
        OutputStream out = null;

            out = new FileOutputStream("config.properties");
            prop.store(out, null);
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    /*
Método que ejecuta una sentencia para insertar un producto en la base de datos con los valores introducidos por parámetro
     */
    public void insertarProducto(String nombreProducto, String proveedor, String tipo, String origen, LocalDate fecha, int intensidad) {
        //preparo la sentencia

        String sentenciaSql = "INSERT INTO productos (nombreProducto,idProveedor,tipo,origen,fechaEmpaquetado,intensidad)" +
        " VALUES (?,?,?,?,?,?)";
    PreparedStatement sentencia=null;

    //para obtener el id del proveedor tenemos que separar el valor que contiene el combobox
    int idProveedor = Integer.valueOf(proveedor.split(" " ) [0]);


        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombreProducto);
            sentencia.setInt(2, idProveedor);
            sentencia.setString(3, tipo);
            sentencia.setString(4, origen);
            sentencia.setDate(5, Date.valueOf(fecha));
            sentencia.setInt(6, intensidad);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }




    }

    /*
 Método que ejecuta una sentencia para insertar una venta en la base de datos con los valores introducidos por parámetro
      */
    public void insertarVenta(String producto, String precio, boolean descuento, String cantidad, LocalDate fecha, String total) {
        String sentenciaSql = "INSERT INTO ventas (idproducto,precio,descuentoSocio,cantidad,total,fechaVenta)" +
                " VALUES (?,?,?,?,?,?)";
        PreparedStatement sentencia=null;

        //para obtener el id del producto tenemos que separar el valor que contiene el combobox
        int idProducto = Integer.valueOf(producto.split(" " ) [0]);


        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idProducto);
            sentencia.setString(2, precio);
            sentencia.setBoolean(3, descuento);
            sentencia.setString(4, cantidad);
            sentencia.setString(5,total);
            sentencia.setDate(6, Date.valueOf(fecha));
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /*
    Método que elimina un proveedor con un id introducido por parámetro
     */
    public void eliminarProveedor(int id) {

        String sentenciaSql = "DELETE FROM proveedores WHERE idProveedor = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, id);
            sentencia.executeUpdate();
        }catch(MySQLIntegrityConstraintViolationException e){
            Util.showWarningAlert("No puedes eliminar este proveedor. Este proveedor está registrado en algún producto");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
    }

    /*
    Método que elimina un producto con un id introducido por parámetro
     */
    public void eliminarProducto(int id) {

        String sentenciaSql = "DELETE FROM productos WHERE idproducto = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, id);
            sentencia.executeUpdate();
        }catch(MySQLIntegrityConstraintViolationException e){
            Util.showWarningAlert("No puedes eliminar este producto. Este producto está registrado en alguna venta.");
        } catch (SQLException e) {

            e.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
    }

    /*
    Método que elimina una venta con un id introducido por parámetro
     */
    public void eliminarVenta(int id) {

        String sentenciaSql = "DELETE FROM ventas WHERE idpedido = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, id);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
    }

    /*
    Método que modifica un proveedor con un id introducido por parámetro
     */
    public void modificarProveedor(String nom  , String desc, String dir, String pais, String tel, int id) {
        String sentenciaSql = "UPDATE proveedores SET nombreProveedor = ?, direccion = ?, pais = ?," +
                "telefono = ?, descripcion = ? WHERE idProveedor = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nom);
            sentencia.setString(2, dir);
            sentencia.setString(3, pais);
            sentencia.setString(4, tel);
            sentencia.setString(5, desc);
            sentencia.setInt(6, id);

            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }




    }


    /*
    Método que modifica un producto con un id introducido por parámetro
     */
    public void modificarProducto(String nombre, String proveedor, String tipo, String origen,
                                  LocalDate fecha, int intensidad, int id) {
        String sentenciaSql = "UPDATE productos SET nombreProducto = ?, idProveedor = ?, tipo = ?," +
                "origen = ?, fechaEmpaquetado = ?, intensidad = ? WHERE idProducto = ?";
        PreparedStatement sentencia = null;

        int idProveedor = Integer.valueOf(proveedor.split(" " ) [0]);


        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setInt(2, idProveedor);
            sentencia.setString(3, tipo);
            sentencia.setString(4, origen);
            sentencia.setDate(5, Date.valueOf(fecha));
            sentencia.setInt(6, intensidad);
            sentencia.setInt(7, id);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }

    }



    /*
    Método que modifica una venta con un id introducido por parámetro
     */
    public void modificarVenta(String producto, String precio, boolean descuento, String cantidad,
                               LocalDate fechaVenta, String total, int id) {

        String sentenciaSql = "UPDATE ventas SET idproducto = ?, precio = ?, descuentoSocio = ?," +
                "cantidad = ?, total = ?, fechaVenta = ? WHERE idpedido = ?";
        PreparedStatement sentencia = null;

        int idProducto = Integer.valueOf(producto.split(" " ) [0]);


        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idProducto);
            sentencia.setString(2, precio);
            sentencia.setBoolean(3, descuento);
            sentencia.setString(4, cantidad);
            sentencia.setString(5,total);
            sentencia.setDate(6, Date.valueOf(fechaVenta));
            sentencia.setInt(7,id);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }
}

