package com.danielgarpal.tiendacafeyte.gui;

import javax.swing.*;
import java.awt.*;
/*
Clase que gestiona las opciones de conexión de este programa. Se abre a través del menú, tras introducir una contraseña
 */
public class ConfigDialog extends JDialog{
    public JPanel panel;
    public JTextField tfIP;
    public JTextField tfUsuario;
    public JPasswordField pfContraseniaAdmin;
    public JPasswordField pfContrasenia;
    public JButton bGuardar;
    public JTextField tfpuerto;


    private Frame owner;


    //Constructor de la clase
    public ConfigDialog(Frame owner){
        super(owner,"Opciones",true);
        this.owner=owner;

        //iniciación del dialog
        initDialog();

    }

    //ajustes de la ventana
    private void initDialog() {
        this.setContentPane(panel);
        this.panel.setBorder(BorderFactory.createEmptyBorder(20,20,20,20));
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.pack();
        this.setSize(new Dimension(this.getWidth()+200,this.getHeight()));
        this.setLocationRelativeTo(owner);
    }


}
